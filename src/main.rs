use gettextrs::{bindtextdomain, setlocale, textdomain, LocaleCategory};
use gtk::{gio, glib};
mod application;
mod config;
mod widgets;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR, RESOURCES_FILE};

fn main() {
    pretty_env_logger::init();

    glib::set_application_name("Obfuscate");
    gtk::init().expect("Unable to start GTK");
    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR).unwrap();
    textdomain(GETTEXT_PACKAGE).unwrap();

    let res = gio::Resource::load(RESOURCES_FILE).expect("Could not load resources");
    gio::resources_register(&res);

    Application::run();
}
