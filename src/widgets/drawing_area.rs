use gdk_pixbuf::prelude::*;
use gtk::{
    gdk, gio,
    glib::{self, clone},
    graphene, gsk,
    prelude::*,
    subclass::prelude::*,
};

use crate::widgets::Window;

#[derive(Debug, Eq, PartialEq, Clone, Copy, glib::Enum)]
#[repr(u32)]
#[enum_type(name = "Filter")]
pub enum Filter {
    Blur = 0,
    Filled = 1,
}

mod imp {
    use super::*;
    use glib::{subclass::Signal, ParamFlags, ParamSpec, ParamSpecBoolean, ParamSpecDouble, ParamSpecEnum, ParamSpecOverride};
    use once_cell::sync::Lazy;
    use std::cell::{Cell, RefCell};

    #[derive(Debug)]
    pub struct DrawingArea {
        pub start_position: RefCell<Option<(f64, f64)>>,
        pub current_position: RefCell<Option<(f64, f64)>>,
        pub pointer_position: RefCell<Option<(f64, f64)>>,
        pub history: RefCell<Vec<gdk::Texture>>,
        pub future: RefCell<Vec<gdk::Texture>>,

        pub initial_zoom: Cell<f64>,
        pub initial_zoom_center: RefCell<Option<(f64, f64)>>,
        pub queued_scroll: RefCell<Option<(f64, f64)>>,
        pub scale_factor: Cell<i32>,

        pub can_undo: Cell<bool>,
        pub can_redo: Cell<bool>,
        pub zoom: Cell<f64>,
        pub filter: Cell<Filter>,

        pub vadjustment: RefCell<Option<gtk::Adjustment>>,
        pub vadjustment_signal: RefCell<Option<glib::SignalHandlerId>>,
        pub hadjustment: RefCell<Option<gtk::Adjustment>>,
        pub hadjustment_signal: RefCell<Option<glib::SignalHandlerId>>,
        pub hscroll_policy: Cell<gtk::ScrollablePolicy>,
        pub vscroll_policy: Cell<gtk::ScrollablePolicy>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DrawingArea {
        const NAME: &'static str = "DrawingArea";
        type ParentType = gtk::Widget;
        type Type = super::DrawingArea;
        type Interfaces = (gtk::Scrollable,);

        fn new() -> Self {
            Self {
                start_position: RefCell::new(None),
                current_position: RefCell::new(None),
                pointer_position: RefCell::new(None),
                history: RefCell::new(Vec::new()),
                future: RefCell::new(Vec::new()),
                initial_zoom: Cell::new(1.0),
                initial_zoom_center: RefCell::new(None),
                queued_scroll: RefCell::new(None),
                scale_factor: Cell::new(1),
                can_redo: Cell::new(false),
                can_undo: Cell::new(false),
                zoom: Cell::new(1.0),
                filter: Cell::new(Filter::Filled),
                hadjustment: RefCell::new(None),
                hadjustment_signal: RefCell::new(None),
                vadjustment: RefCell::new(None),
                vadjustment_signal: RefCell::new(None),
                hscroll_policy: Cell::new(gtk::ScrollablePolicy::Minimum),
                vscroll_policy: Cell::new(gtk::ScrollablePolicy::Minimum),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            klass.set_accessible_role(gtk::AccessibleRole::Img);
        }
    }

    impl ObjectImpl for DrawingArea {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| vec![Signal::builder("loaded", &[], <()>::static_type().into()).build()]);
            SIGNALS.as_ref()
        }

        fn constructed(&self, obj: &Self::Type) {
            obj.set_hexpand(true);
            obj.set_vexpand(true);
            obj.set_overflow(gtk::Overflow::Hidden);

            if let Some(cursor) = gdk::Cursor::from_name("crosshair", None) {
                obj.set_cursor(Some(&cursor));
            }

            obj.connect_scale_factor_notify(clone!(@weak obj => move |_| {
                let imp = obj.imp();
                let change = obj.scale_factor() as f64 / imp.scale_factor.get() as f64;

                let hadj = obj.hadjustment().unwrap();
                let vadj = obj.vadjustment().unwrap();
                let new_scroll = (hadj.value(), vadj.value());

                obj.set_property("zoom", &(imp.zoom.get() * change));
                imp.queued_scroll.replace(Some(new_scroll));

                imp.scale_factor.set(obj.scale_factor());

                obj.queue_allocate();
            }));

            let gesture_drag = gtk::GestureDrag::new();
            gesture_drag.set_exclusive(true);
            gesture_drag.connect_drag_begin(clone!(@weak obj => move |_, start_x, start_y| {
                let image_coords = obj.to_image_coords((start_x, start_y));
                obj.imp().start_position.replace(Some(image_coords));
            }));
            gesture_drag.connect_drag_update(clone!(@weak obj => move |gesture, offset_x, offset_y| {
                if let Some(start_point) = gesture.start_point() {
                    let (start_x, start_y) = start_point;
                    let image_coords = obj.to_image_coords((start_x + offset_x, start_y + offset_y));
                    obj.imp().current_position.replace(Some(image_coords));
                    obj.queue_draw();
                }
            }));
            gesture_drag.connect_drag_end(clone!(@weak obj => move |gesture, offset_x, offset_y| {
                if let Some(start_point) = gesture.start_point() {
                    let (start_x, start_y) = start_point;
                    let image_coords = obj.to_image_coords((start_x + offset_x, start_y + offset_y));
                    obj.imp().current_position.replace(Some(image_coords));
                    obj.apply_filter();
                }
            }));
            gesture_drag.connect_cancel(clone!(@weak obj => move |_, _| {
                let imp = obj.imp();
                imp.start_position.replace(None);
                imp.current_position.replace(None);
            }));
            obj.add_controller(&gesture_drag);

            let gesture_zoom = gtk::GestureZoom::new();
            gesture_zoom.connect_begin(clone!(@weak obj => move |gesture, _| {
                let view_center = gesture.bounding_box_center().unwrap();
                obj.begin_zoom(view_center);

                gesture.set_state(gtk::EventSequenceState::Claimed);
            }));
            gesture_zoom.connect_scale_changed(clone!(@weak obj => move |gesture, scale| {
                let view_center = gesture.bounding_box_center().unwrap();
                obj.set_zoom_at_center(obj.imp().initial_zoom.get() * scale, view_center);
            }));
            obj.add_controller(&gesture_zoom);

            let motion_controller = gtk::EventControllerMotion::new();
            motion_controller.connect_enter(clone!(@weak obj => move |_, x, y| {
                obj.imp().pointer_position.replace(Some((x, y)));
            }));
            motion_controller.connect_motion(clone!(@weak obj => move |_, x, y| {
                obj.imp().pointer_position.replace(Some((x, y)));
            }));
            motion_controller.connect_leave(clone!(@weak obj => move |_| {
                obj.imp().pointer_position.replace(None);
            }));
            obj.add_controller(&motion_controller);

            let scroll_controller = gtk::EventControllerScroll::new(gtk::EventControllerScrollFlags::VERTICAL);
            scroll_controller.connect_scroll(clone!(@weak obj => @default-panic, move |event, _delta_x, delta_y| {
                if event.current_event_state().contains(gdk::ModifierType::CONTROL_MASK) {
                    let zoom = - delta_y * 0.1 + obj.zoom();
                    obj.set_zoom(zoom);
                    gtk::Inhibit(true)
                } else {
                    gtk::Inhibit(false)
                }
            }));
            obj.add_controller(&scroll_controller);

            let drop_target = gtk::DropTarget::new(gio::File::static_type(), gdk::DragAction::COPY);
            drop_target.connect_drop(glib::clone!(@weak obj => @default-return false, move |_, value, _, _| {
                if let Ok(file) = value.get::<gio::File>() {
                    let window = obj.root().unwrap().downcast::<Window>().unwrap();
                    window.open_file(&file);
                    true
                } else {
                    false
                }
            }));
            obj.add_controller(&drop_target);
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecBoolean::new("can-undo", "Can undo", "whether you can undo", false, ParamFlags::READWRITE),
                    ParamSpecBoolean::new("can-redo", "Can redo", "whether you can redo", false, ParamFlags::READWRITE),
                    ParamSpecDouble::new("zoom", "Zoom", "Current zoom level", 0.0, f64::INFINITY, 1.0, ParamFlags::READWRITE),
                    ParamSpecEnum::new("filter", "Filter", "The applied filter", Filter::static_type(), Filter::Filled as i32, ParamFlags::READWRITE),
                    // Scrollable properties
                    ParamSpecOverride::for_interface::<gtk::Scrollable>("hscroll-policy"),
                    ParamSpecOverride::for_interface::<gtk::Scrollable>("vscroll-policy"),
                    ParamSpecOverride::for_interface::<gtk::Scrollable>("hadjustment"),
                    ParamSpecOverride::for_interface::<gtk::Scrollable>("vadjustment"),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, obj: &Self::Type, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                "can-undo" => {
                    let can_undo = value.get().unwrap();
                    self.can_undo.set(can_undo);
                }
                "can-redo" => {
                    let can_redo = value.get().unwrap();
                    self.can_redo.set(can_redo);
                }
                "zoom" => {
                    let zoom = value.get().unwrap();
                    self.zoom.set(zoom);
                }
                "filter" => {
                    let filter = value.get().unwrap();
                    self.filter.set(filter);
                }
                "hadjustment" => {
                    let hadjustment = value.get().unwrap();
                    obj.set_hadjustment(hadjustment);
                }
                "hscroll-policy" => {
                    let hscroll_policy = value.get().unwrap();
                    self.hscroll_policy.replace(hscroll_policy);
                }
                "vadjustment" => {
                    let vadjustment = value.get().unwrap();
                    obj.set_vadjustment(vadjustment);
                }
                "vscroll-policy" => {
                    let vscroll_policy = value.get().unwrap();
                    self.vscroll_policy.replace(vscroll_policy);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                "can-undo" => self.can_undo.get().to_value(),
                "can-redo" => self.can_redo.get().to_value(),
                "zoom" => self.zoom.get().to_value(),
                "filter" => self.filter.get().to_value(),
                "hadjustment" => self.hadjustment.borrow().to_value(),
                "vadjustment" => self.vadjustment.borrow().to_value(),
                "hscroll-policy" => self.hscroll_policy.get().to_value(),
                "vscroll-policy" => self.vscroll_policy.get().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for DrawingArea {
        fn measure(&self, widget: &Self::Type, orientation: gtk::Orientation, _for_size: i32) -> (i32, i32, i32, i32) {
            let zoom = widget.effective_zoom();
            let (width, height) = if let Some(texture) = self.history.borrow().iter().last() {
                ((texture.width() as f64 * zoom) as i32, (texture.height() as f64 * zoom) as i32)
            } else {
                // some default (width, height)
                (300, 300)
            };

            if orientation == gtk::Orientation::Horizontal {
                (0, width, -1, -1)
            } else {
                (0, height, -1, -1)
            }
        }

        fn snapshot(&self, widget: &Self::Type, snapshot: &gtk::Snapshot) {
            if let Some(texture) = widget.texture() {
                let hadj = widget.hadjustment().unwrap();
                let vadj = widget.vadjustment().unwrap();
                let zoom = widget.effective_zoom() as f32;
                let (translate_x, translate_y) = translate(widget.width() as f32, widget.height() as f32, &texture, zoom);

                snapshot.save();
                snapshot.translate(&graphene::Point::new((-hadj.value() as f32 + translate_x).round(), (-vadj.value() as f32 + translate_y).round()));
                snapshot.scale(zoom, zoom);
                snapshot.append_texture(&texture, &widget.bounds());
                snapshot.restore();

                if let Some(end_pos) = *self.current_position.borrow() {
                    let start_pos = *self.start_position.borrow().as_ref().unwrap();

                    let view_start = widget.convert_image_coords(start_pos);
                    let view_end = widget.convert_image_coords(end_pos);

                    let (x, y) = top_left_corner(view_start, view_end);
                    let (width, height) = width_height(view_start, view_end);

                    let selection_rect = graphene::Rect::new(x, y, width, height);

                    snapshot.append_border(
                        &gsk::RoundedRect::from_rect(selection_rect, 0.0),
                        &[1.0, 1.0, 1.0, 1.0],
                        &[gdk::RGBA::BLACK, gdk::RGBA::BLACK, gdk::RGBA::BLACK, gdk::RGBA::BLACK],
                    );
                }
            }
        }

        fn size_allocate(&self, widget: &Self::Type, width: i32, height: i32, _baseline: i32) {
            if let Some(texture) = widget.texture() {
                let zoom = widget.effective_zoom();

                let hadjustment = widget.hadjustment().unwrap();
                let vadjustment = widget.vadjustment().unwrap();

                let (x, y) = self.queued_scroll.take().unwrap_or((hadjustment.value(), vadjustment.value()));

                hadjustment.configure(x, 0.0, (width as f64).max(texture.width() as f64 * zoom), 0.1 * width as f64, 0.9 * width as f64, width as f64);
                vadjustment.configure(y, 0.0, (height as f64).max(texture.height() as f64 * zoom), 0.1 * height as f64, 0.9 * height as f64, height as f64);
            }
        }
    }

    impl ScrollableImpl for DrawingArea {}
}

glib::wrapper! {
    pub struct DrawingArea(ObjectSubclass<imp::DrawingArea>)  @extends gtk::Widget, @implements gtk::Scrollable;
}

impl DrawingArea {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        glib::Object::new(&[]).unwrap()
    }

    pub fn zoom(&self) -> f64 {
        self.property("zoom")
    }

    fn effective_zoom(&self) -> f64 {
        self.zoom() / self.scale_factor() as f64
    }

    fn set_hadjustment(&self, adj: Option<gtk::Adjustment>) {
        let imp = self.imp();
        if let Some(signal_id) = imp.hadjustment_signal.borrow_mut().take() {
            let old_adj = imp.hadjustment.borrow().as_ref().unwrap().clone();
            old_adj.disconnect(signal_id);
        }

        if let Some(ref adjustment) = adj {
            let signal_id = adjustment.connect_value_changed(clone!(@weak self as this => move |_| {
                this.queue_draw();
            }));
            imp.hadjustment_signal.replace(Some(signal_id));
        }
        imp.hadjustment.replace(adj);
    }

    fn set_vadjustment(&self, adj: Option<gtk::Adjustment>) {
        let imp = self.imp();
        if let Some(signal_id) = imp.vadjustment_signal.borrow_mut().take() {
            let old_adj = imp.vadjustment.borrow().as_ref().unwrap().clone();
            old_adj.disconnect(signal_id);
        }

        if let Some(ref adjustment) = adj {
            let signal_id = adjustment.connect_value_changed(clone!(@weak self as this => move |_| {
                this.queue_draw();
            }));
            imp.vadjustment_signal.replace(Some(signal_id));
        }
        imp.vadjustment.replace(adj);
    }

    pub fn load_file(&self, file: &gio::File) -> anyhow::Result<()> {
        let texture = gdk::Texture::from_file(file)?;
        self.load_texture(texture);
        Ok(())
    }

    fn texture(&self) -> Option<gdk::Texture> {
        let history = self.imp().history.borrow().clone();
        history.into_iter().last()
    }

    fn bounds(&self) -> graphene::Rect {
        let texture = self.texture().unwrap();
        graphene::Rect::new(0.0, 0.0, texture.width() as f32, texture.height() as f32)
    }

    fn load_texture(&self, texture: gdk::Texture) {
        let imp = self.imp();
        imp.history.replace(vec![texture]);
        imp.future.replace(Vec::new());
        imp.start_position.replace(None);
        imp.current_position.replace(None);
        imp.initial_zoom_center.replace(None);
        self.set_property("can-undo", &false);
        self.set_property("can-redo", &false);
        self.set_zoom(1.0);
        imp.queued_scroll.replace(Some((0.0, 0.0)));
        self.queue_allocate();
    }

    fn begin_zoom(&self, zoom_center: (f64, f64)) {
        let imp = self.imp();
        imp.initial_zoom.set(imp.zoom.get());
        imp.initial_zoom_center.replace(Some(self.to_image_coords(zoom_center)));
    }

    pub fn set_zoom_at_center(&self, new_zoom: f64, zoom_center: (f64, f64)) {
        let imp = self.imp();

        let zoom = imp.zoom.get();
        self.set_property("zoom", &new_zoom.clamp((0.1f64).min(zoom), (8.0f64).max(zoom)));

        let hadj = self.hadjustment().unwrap();
        let vadj = self.vadjustment().unwrap();

        let (center_x, center_y) = zoom_center;
        let (x, y) = self.convert_image_coords(imp.initial_zoom_center.borrow().unwrap());

        let new_scroll = (x + hadj.value() - center_x, y + vadj.value() - center_y);
        imp.queued_scroll.replace(Some(new_scroll));

        self.queue_allocate();
        self.queue_draw();
    }

    pub fn set_zoom(&self, new_zoom: f64) {
        if let Some(pos) = *self.imp().pointer_position.borrow() {
            self.begin_zoom(pos);
            self.set_zoom_at_center(new_zoom, pos);
            return;
        }

        let width = self.width();
        let height = self.height();
        let pos = (width as f64 / 2.0, height as f64 / 2.0);
        self.begin_zoom(pos);
        self.set_zoom_at_center(new_zoom, pos);
    }

    pub fn set_filter(&self, filter: Filter) {
        self.set_property("filter", &filter);
    }

    fn filter(&self) -> Filter {
        self.property("filter")
    }

    pub fn save_to(&self, dest: &gio::File) -> anyhow::Result<()> {
        if let Some(ref texture) = self.texture() {
            let pixbuf = gdk::pixbuf_get_from_texture(texture).unwrap();
            let stream = dest.replace(None, false, gio::FileCreateFlags::REPLACE_DESTINATION, gio::Cancellable::NONE)?;
            pixbuf.save_to_streamv(&stream, "png", &[], gio::Cancellable::NONE)?;
        }
        Ok(())
    }

    pub fn paste(&self) {
        let display = gdk::Display::default().unwrap();
        let clipboard = display.clipboard();
        clipboard.read_texture_async(
            gio::Cancellable::NONE,
            clone!(@weak self as this => move |r| if let Ok(Some(texture)) = r {
                this.load_texture(texture);
                this.emit_by_name::<()>("loaded", &[]);
            }),
        );
    }

    pub fn copy(&self) {
        if let Some(ref texture) = self.texture() {
            let display = gdk::Display::default().unwrap();
            let clipboard = display.clipboard();

            clipboard.set_texture(texture);
        }
    }

    pub fn undo(&self) {
        let imp = self.imp();
        if let Some(texture) = imp.history.borrow_mut().pop() {
            self.set_property("can-redo", &true);
            imp.future.borrow_mut().push(texture);
        } else {
            self.set_property("can-redo", &false);
        }
        self.set_property("can-undo", &(imp.history.borrow().len() > 1));
        self.queue_allocate();
        self.queue_draw();
    }

    pub fn redo(&self) {
        let imp = self.imp();
        if let Some(texture) = imp.future.borrow_mut().pop() {
            imp.history.borrow_mut().push(texture);
            self.set_property("can-undo", &true);
        } else {
            self.set_property("can-undo", &true);
        }
        self.set_property("can-redo", &(imp.future.borrow().len() != 0));
        self.queue_draw();
    }

    /// Takes current (start_position, current_position)
    /// and based on the selected filter, create an appropriate texture
    /// and add it to the history
    fn apply_filter(&self) {
        let imp = self.imp();
        let start_pos = imp.start_position.take().unwrap();
        let end_pos = imp.current_position.take().unwrap();
        let texture = self.texture().unwrap();

        let start_pos = self.clamp_to_image(start_pos);
        let end_pos = self.clamp_to_image(end_pos);

        let (x, y) = top_left_corner(start_pos, end_pos);
        let (width, height) = width_height(start_pos, end_pos);

        let filter_bounds = graphene::Rect::new(x, y, width, height);
        let texture_bounds = self.bounds();

        let renderer = self.root().unwrap().upcast::<gtk::Native>().renderer();
        let snapshot = gtk::Snapshot::new();
        match self.filter() {
            Filter::Blur => {
                snapshot.append_texture(&texture, &texture_bounds);
                snapshot.push_clip(&filter_bounds);
                snapshot.push_blur(10.0);
                snapshot.push_clip(&filter_bounds.inset_r(-10.0, -10.0));
                snapshot.append_texture(&texture, &texture_bounds);
                snapshot.pop();
                snapshot.pop();
                snapshot.pop();
            }
            Filter::Filled => {
                snapshot.append_texture(&texture, &texture_bounds);
                snapshot.append_color(&gdk::RGBA::BLACK, &filter_bounds);
            }
        };

        let node = snapshot.to_node().unwrap();
        let texture = renderer.render_texture(&node, None);

        imp.history.borrow_mut().push(texture);
        imp.current_position.replace(None);
        self.set_property("can-undo", &true);
        self.queue_draw();
    }

    fn to_image_coords(&self, view_coords: (f64, f64)) -> (f64, f64) {
        let hadj = self.hadjustment().unwrap();
        let vadj = self.vadjustment().unwrap();
        let texture = self.texture().unwrap();
        let zoom = self.effective_zoom();
        let (translate_x, translate_y) = translate(self.width() as f32, self.height() as f32, &texture, zoom as f32);
        let (x, y) = view_coords;

        let image_x = (x + hadj.value() - translate_x as f64) / zoom;
        let image_y = (y + vadj.value() - translate_y as f64) / zoom;

        (image_x, image_y)
    }

    fn convert_image_coords(&self, image_coords: (f64, f64)) -> (f64, f64) {
        let hadj = self.hadjustment().unwrap();
        let vadj = self.vadjustment().unwrap();
        let texture = self.texture().unwrap();
        let zoom = self.effective_zoom();
        let (translate_x, translate_y) = translate(self.width() as f32, self.height() as f32, &texture, zoom as f32);
        let (x, y) = image_coords;

        let view_x = x * zoom - hadj.value() + translate_x as f64;
        let view_y = y * zoom - vadj.value() + translate_y as f64;

        (view_x, view_y)
    }

    fn clamp_to_image(&self, pos: (f64, f64)) -> (f64, f64) {
        let texture = self.texture().unwrap();
        let (x, y) = pos;

        let x = x.clamp(0.0, texture.width() as f64);
        let y = y.clamp(0.0, texture.height() as f64);

        (x, y)
    }
}

fn top_left_corner(pos1: (f64, f64), pos2: (f64, f64)) -> (f32, f32) {
    let (x0, y0) = pos1;
    let (x1, y1) = pos2;

    let width = x1 - x0;
    let height = y1 - y0;
    // figure out the real start position of the rectangle
    let (x, y) = if height < 0.0 && width > 0.0 {
        (x0, y1)
    } else if width < 0.0 && height > 0.0 {
        (x1, y0)
    } else if width < 0.0 && height < 0.0 {
        (x1, y1)
    } else {
        (x0, y0)
    };
    (x as f32, y as f32)
}

fn width_height(pos1: (f64, f64), pos2: (f64, f64)) -> (f32, f32) {
    let (x0, y0) = pos1;
    let (x1, y1) = pos2;

    let width = (x1 - x0).abs();
    let height = (y1 - y0).abs();

    ((width) as f32, (height) as f32)
}

fn translate(width: f32, height: f32, texture: &gdk::Texture, zoom: f32) -> (f32, f32) {
    let (mut translate_x, mut translate_y) = (0.0, 0.0);
    let texture_width = texture.width() as f32 * zoom;
    let texture_height = texture.height() as f32 * zoom;

    if width > texture_width {
        translate_x = (width - texture_width) / 2.0;
    }
    if height > texture_height {
        translate_y = (height - texture_height) / 2.0;
    }
    (translate_x, translate_y)
}
