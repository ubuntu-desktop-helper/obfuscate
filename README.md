<a href="https://flathub.org/apps/details/com.belmoussaoui.Obfuscate">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Obfuscate

<img src="https://gitlab.gnome.org/World/obfuscate/raw/master/data/icons/com.belmoussaoui.Obfuscate.svg" width="128" height="128" />

Censor private information

## Screenshots

<div align="center">
![screenshot](data/resources/screenshots/screenshot1.png)
</div>

## Hack on Obfuscate
To build the development version of Obfuscate and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow the GNOME Code of Conduct when participating in project
spaces.
